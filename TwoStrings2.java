class TwoStrings2 {
	synchronized static void print(String str1, String str2) {
		System.out.print(str1);
		try {
			Thread.sleep(500);
		} catch (InterruptedException ie) {
		}
		System.out.println(str2);
	}
	public static void main(String args[]) {
		new PrintStringThread("Hello ", "there.");
		new PrintStringThread("How are", "you?");
		new PrintStringThread("Thank you", "very much!");
	}

}
class PrintStringThread implements Runnable {
	Thread thread;
	String str1, str2;
	PrintStringThread(String str1, String str2) {
		this.str1 = str1;
		this.str2 = str2;
		thread = new Thread(this);
		thread.start();
	}
	public void run() {
		TwoStrings2.print(str1, str2);
	}
}
